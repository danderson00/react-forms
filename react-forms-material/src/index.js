import { createElement } from 'react'
import { FormProvider, context, createRawConsumer, wrapInput, wrapSubmit, useFormValues } from '@danderson00/react-forms'
import { extractErrorMessage } from '@danderson00/react-forms/dist/ErrorMessage'
import MuiButton from '@mui/material/Button'
import MuiCheckbox from '@mui/material/Checkbox'
import MuiFormControl from '@mui/material/FormControl'
import MuiFormHelperText from '@mui/material/FormHelperText'
import MuiInput from '@mui/material/Input'
import MuiInputLabel from '@mui/material/InputLabel'
import MuiMenuItem from '@mui/material/MenuItem'
import MuiRadio from '@mui/material/Radio'
import MuiRadioGroup from '@mui/material/RadioGroup'
import MuiSelect from '@mui/material/Select'
import './styles'

export const ErrorMessage = ({ error }) => createElement(MuiFormHelperText, { children: extractErrorMessage(error), error: true })
export const Form = FormProvider(context.Provider, { margin: 'dense' }, ErrorMessage)
export const Submit = wrapSubmit(MuiButton)
export const Button = MuiButton
export { useFormValues }

const wrapped = (component, options, baseOptions) => wrapInput(
  base(component, options, baseOptions),
  { passErrorProp: true, ...options }
)

const base = (component, options = {}, { alwaysShrinkLabel, shrinkLabel: shrinkLabelOption = true, passError } = {}) => (
  ({ className, error, hiddenLabel, variant, margin, label, helperText, shrinkLabel, valid, ...inputProps }) => {
    return (
      createElement(MuiFormControl, {
        className: `react-forms-material-field${options.type ? ` react-forms-material-${options.type}` : ''}${className ? ` ${className}` : ''}`,
        error,
        fullWidth: inputProps.fullWidth,
        hiddenLabel,
        required: inputProps.required,
        variant,
        margin,
        children: [
          ...(label ? [createElement(MuiInputLabel, {
            key: 'label',
            htmlFor: inputProps.id,
            children: label,
            ...(shrinkLabelOption === false && { shrink: false }),
            ...((alwaysShrinkLabel || shrinkLabel) && { className: 'MuiInputLabel-shrink' }),
          })] : []),

          createElement(component, {
            key: 'input',
            variant,
            ...(passError && { error }),
            ...inputProps
          }),

          ...(helperText ? [createElement(MuiFormHelperText, {
            key: 'helperText',
            children: helperText
          })] : [])
        ]
      })
    )
  }
)

export const Input = wrapInput(MuiInput)
export const Text = wrapped(MuiInput)
export const Checkbox = wrapped(MuiCheckbox, { type: 'checkbox' }, { alwaysShrinkLabel: true })
export const RadioButton = wrapped(MuiRadio, { type: 'radio' }, { shrinkLabel: false })

export const Radio = createRawConsumer(base(
  ({ name, values, labels, numeric, required, getFieldValue, getFieldValues, ...props }) => (
    createElement(MuiRadioGroup, { ...props, children: [
      values.map((value, index) =>
        createElement(RadioButton, {
          name,
          value,
          numeric,
          key: value,
          label: (labels && labels[index]) || value,
          margin: props.margin,
          ...(required && { required })
        })
      )
    ] })
  ),
  { type: 'radiogroup' },
  { alwaysShrinkLabel: true  }
))

export const Select = wrapped(({ values, labels, ...props }) =>
  createElement(MuiSelect, { ...props, children:
    values.map((value, index) =>
      createElement(MuiMenuItem, {
        value,
        key: value,
        children: (labels && labels[index]) || value,
        ...(props.required && { required: props.required })
      })
    )
  }),
  { type: 'select' },
  { alwaysShrinkLabel: true  }
)

export const wrapField = wrapped