import injectCss from '@danderson00/react-forms/dist/injectCss'

injectCss('react-forms-material-styles', `
  .react-forms-material-radiogroup.MuiFormControl-root {
    overflow: hidden;
  }

  .react-forms-material-radio, 
  .react-forms-material-checkbox {
    display: flex !important;
    flex-direction: row-reverse !important;
    align-items: center;
  }

  .react-forms-material-radio.MuiFormControl-root {
    flex-direction: row-reverse;
    justify-content: flex-end;
  }
  
  .react-forms-material-radio .MuiInputLabel-formControl, 
  .react-forms-material-radiogroup .MuiInputLabel-formControl,
  .react-forms-material-checkbox .MuiInputLabel-formControl {
    position: static;
    transform: none;
    line-height: 1;
    transform: translateY(2px) scale(1);
  }
  
  
  .react-forms input::-webkit-outer-spin-button,
  .react-forms input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  .react-forms input[type=number] {
    -moz-appearance: textfield;
  }
  
  ${''/* allow forms to be embedded in dialogs - essentially replicate a few styles */}
  .MuiDialog-container form {
    display: flex;
  }
  
  .MuiDialog-container form .MuiDialogContent-root {
    display: flex;
    align-items: flex-start;
    flex-direction: column;
  }

  .MuiDialog-container form .MuiDialogActions-root {
    align-self: flex-end;
  }
`)


