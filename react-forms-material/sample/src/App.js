import { SampleForm } from './Form'

export default function App() {
  return <>
    <SampleForm onSubmit={values => {
      alert(JSON.stringify(values, null, 2))
    }} />
    <small>github repository at <a href="https://github.com/danderson00/react-forms-material">https://github.com/danderson00digital/react-forms-material</a></small>
  </>
}
