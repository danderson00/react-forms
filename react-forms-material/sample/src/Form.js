import { Form, Text, Select, Radio, Checkbox, Submit, wrapField } from '@danderson00/react-forms-material'
import { Switch } from '@mui/material'

const WrappedSwitch = wrapField(Switch, { type: 'checkbox' })

const ErrorInput = wrapField(({ error, ...props }) => (
  <input {...props} style={error ? { background: 'red' } : undefined} />
), {}, { passError: true })

export const SampleForm = ({ onSubmit, initialValues }) => (
  <Form onSubmit={onSubmit} values={initialValues} margin="dense">
    <Text name="name" label="Name" required minLength="5" maxLength="50" variant="outlined" />
    <Text name="number" label="Number" required numeric />
    <Text name="description" label="Description" multiline rows={4} maxLength="100" />
    <Select name="type" label="Type" values={['', 'Widget', 'Component']} required />
    <Radio name="rating" label="Rating" values={['Some word', 'Another word', 'Single']} required />
    <Radio name="rating" label="Rating" values={[1, 2, 3]} row />
    <Checkbox name="urgent" label="Urgent" />
    <WrappedSwitch name="ffs" />
    <ErrorInput name="error" required />

    <Submit>Submit</Submit>
  </Form>
)